#!/bin/sh

maildir=/tmp/public

ls=/bin/ls
expr=expr

mailperms=`$ls -lgd $maildir/.`

mailpublic=`$expr "$mailperms" : "d[rwxstT]*\(w[tx]\) .*"`

echo mailpublic is $mailpublic

case "$mailpublic" in
   wt)
       echo has writable for all
       d_setgid="$undef"
       ;;
   wx)
       echo "WARNING: mail directory does not have 't' flag set, yet is world-writable"
       echo "$mailperms"
       d_setgid="$undef"
       ;;
   "")
       echo presume it doesnt exist yet
       ;;
   *)
       echo does NOT have writable thing
       d_setgid="$define"
       ;;
esac

echo d_setgid is "$d_setgid"
