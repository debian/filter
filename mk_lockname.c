
static char rcsid[] = "@(#)$Id: mk_lockname.c,v 5.1 1992/10/03 22:41:36 syd Exp $";

/*******************************************************************************
 *  The Elm Mail System  -  $Revision: 5.1 $   $State: Exp $
 *
 *			Copyright (c) 1988-1992 USENET Community Trust
 *			Copyright (c) 1986,1987 Dave Taylor
 *******************************************************************************
 * Bug reports, patches, comments, suggestions should be sent to:
 *
 *	Philip Brown    filter@bolthole.com
 *
 *******************************************************************************
 * $Log: mk_lockname.c,v $
 * Revision 5.1  1992/10/03  22:41:36  syd
 * Initial checkin as of 2.4 Release at PL0
 *
 *
 ******************************************************************************/

#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include "defs.h"

static char lock_name[SLEN];

/**
 * Create the proper name of the lock file for file_to_lock,
 * which is presumed to be a spool file full path (see
 * get_folder_type()), and put it in the static area lock_name.
 * This is special for "mailspool" folders, because we have to
 * create the lockfile in /tmp. Otherwise, we should just use
 * the same directory as the folder.
 * But unless we are in XENIX, its up to the caller to pass in /tmp/xxx
 *
 * Return lock_name for informational purposes.
**/
char * mk_lockname(file_to_lock)
char *file_to_lock;
{

#ifdef XENIX
        /* lock needs to be /tmp/[basename of file_to_lock].mlk */
        sprintf(lock_name, "/tmp/%.10s.mlk", rindex(file_to_lock, '/')+1);
#else
        /* lock is [file_to_lock].lock */
        sprintf(lock_name, "%s.lock", file_to_lock);
#endif
        return(lock_name);
}

