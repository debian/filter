.if n \{\
.	ds ct "
.\}
.if t \{\
.	ds ct \\(co
.\}
.TH FILTER 1 "19 April 2004" "USENET Comm. Trust"
.SH NAME
filter - filter incoming messages before adding to mailbox
.SH SYNOPSIS
.B filter
[-q] [-v] [-l] [-o output] [-f rules-file] [-m mailbox]
.br
.B filter
[-n] [-f rules-file]
.br
.B filter
[-r] [-o output] [-f rules-file]
.br
.B filter
[-c] [-s] [-o output] [-f rules-file]
.br
.B filter
[-c] [-S] [-o output] [-f rules-file]

.SH DESCRIPTION
In brief, \fIfilter\fR is designed to sort incoming mail based on user
preferences for various recognized headers.
It logs a summary of messages filtered to a logfile.
See the USAGE section for full details.

.SH OPTIONS
The flags the \fIfilter\fR program understands are;
.TP 1.0i
.B "-c"
Clear logs.  If this flag is used, the log files will be removed after
being summarized by either "-s" or "-S" (see below).
.TP
\fB-f \fIrules-file\fR
Get rules from specified file instead of default $HOME/.filter/filter-rules
.TP
.B "-l"
Log actions only.  Do not log information about mailed messages.
.TP
.B "-n"
Not really.  Output what would happen if given message from standard
input, but don't actually do anything with it.
.TP
\fB-o \fIfile\fR
Redirect stdout messages to the specified file or device.
Logging of filter actions is not affected.  (This is
added to more gracefully deal with changes in the file system (since
sendmail is quite picky about modes of files being redirected into.))
.TP
\fB-m \fImailbox\fR
Override the compiled in definition of your primary 'mailbox'.
This is disabled if filter is setgid.
.TP
.B "-q"
Quiet.  Do not log filtering actions.
.TP
.B "-r"
Rules.  List the rules currently being used.
.TP
.B "-s"
Summarize.  List a summary of the message filtering log.
.TP
.B "-S"
This is the same as `-s' but includes message-by-message
information additionally.
.TP
.B "-v"
Verbose.  Give output on standard out for each message filtered.  May be useful
to set output to ``/dev/console'' or other logging device.  (see \fB-o\fR).


.SH USAGE
.I Filter
uses a set of selection rules to process incoming mail messages. These
rules are normally read in from $HOME/.filter/filter-rules. As soon as a rule
matches something about the message, that rule is used, and no further
action is taken.
The general format for a rule is;
.nf
        
        \fBif  (\fIexpression\fP)  then  \fIaction\fR

.fi
where \fIexpression\fR is:
.nf

        \fIexpression\fR  ::=  { \fBnot\fR }  \fIcondition\fR  { and \fIexpression\fR }

        \fIcondition\fR  ::=  \fIfield  =  stringvalue\fR
		         ::=  \fIfield  != stringvalue\fR
                   ::=  \fBlines\fI  relop  numvalue\fR
or                 ::=  \fBalways\fR

.fi
These further break down as:
.nf

        \fIfield\fR  ::=  \fBfrom\fR  |  \fBsubject\fR  |  \fBalphasubject\fR  |  \fBto\fR  |  \fBlines\fR | \fBsender\fR

        \fIrelop\fR  ::=  \fB=\fR  |  \fB!=\fR  |  \fB<\fR  |  \fB>\fR  |  \fB<=\fR  |  \fB>=\fR

        \fIstringvalue\fR  ::=  \fIany  quoted  string\fR

        \fInumvalue\fR  ::=  \fIany  integer  value\fR

.fi

Note: \fBalphasubject\fR is a transformed version of the subject line, where only 
alpha chars are preserved, and they are forced to lower case. It is then 
simpler to use a regular expression match across a wide variety of 
similar subjects.

\fBfrom\fR matches any and all of the following headers:
  "From " From: Reply-To: Sender:

\fIAction\fR can be any of:
.nf

    \fBdelete\fR              (ignore this message; throw it away)
    \fBsave  \fIfoldername\fR    (put in 'foldername' for later)
    \fBsavecopy \fIfoldername\fR (save a copy AND put in my inbox)
    \fBexecute  \fIcommand\fR    (pipe message to 'command')
    \fBexecutec \fIcommand\fR    (copy to inbox, AND pipe to 'command') 
    \fBforward  \fIaddress\fR    (forward this message)
    \fBforwardc \fIaddress\fR    (forward this message, AND copy to inbox)
    \fBresend   \fIaddress\fR    (resend to new addr without change)
    \fBleave\fR               (just put it in my inbox)
    \fBbounce\fR              (say there's no such user)

.fi

An example of a rules file would be:

.nf
    # I'll read this stuff later when I feel like it
    if (from = "list@interest.org") then save "~/Mail/mailinglist"
    # This mailing list actually has an x-mailing-list header
    if (mailinglist = "smartlist@other.com") then save "~/Mail/otherlist"
    # auto-archive this project's email, AND put in my current mailbox
    if (subject = "strange project") then savecopy "~/Mail/proj-archive"
    # If "make money" appears ANYWHERE in subject, upper/lowercase,
    # delete the lousy spam
    if ( subject = "make money" ) then delete
    # This person no longer shares this virtual mailbox with me.
    # Resend visibly as a "forwarded" message.
    if (to = "partner") then forward "partner@new.address.com"
    # If email for an old address comes to me, resend to new one.
    # Note that the "envelope" will show my address, but the
    # normal headers will not
    if ( to = "old_address" ) then resend new@address.here"
    # Special virtual headerline derived from normal subject line
    # catches "multiple!!!wor+ds**here"
    if ( alphasubject = "multiplewordshere" ) then delete
    # If email for a special address comes to me, resend through
    # a non-standard "special" mailer
    if ( to = "special_address" ) then exec "/usr/lib/mailer special@addr"
    #
    # The rules file doesnt really need parentheses. Or even the 'if'.
    # They are just traditional.
    subject = "silly" then bounce
.fi

Note that all filenames must be double-quoted

For further information about the rules language, please see
\fIThe Elm Filter Guide\fR.
.sp

.SH MAINTAINER
Philip Brown
.SH FILES
$HOME/.filter/filter-rules Default rules file
.br
$HOME/.filter/filterlog    A log of what has been done
.br
$HOME/.filter/filtersum    A summary of what has been done
.br
$MAIL                   IF you set this var, defines your
                        default mailbox
.br
/etc/passwd             Used to get users home directory
.SH SEE ALSO
.I "The Elm Filter Guide"
(distributed with the source code)
.br
mail(1), mailx(1), sendmail(1,8)
.SH COMMENTS
Could be smarter about pattern matching. Message body checks might be nice too.

.br
Please note that this was historically bundled with the elm mail reader,
but now can be found at 
.br
http://www.bolthole.com/filter/
.SH BUG REPORTS TO
filter@bolthole.com
.SH COPYRIGHTS
\fB\*(ct\fRCopyright 1988-1992 by The USENET Community Trust
.br
Derived from Elm 2.0, \fB\*(ct\fR Copyright 1986, 1987 by Dave Taylor
