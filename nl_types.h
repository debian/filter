/*
 * Just a basic nl_types bypass file, which creates the minimal source
 * fakery needed to compile the filter program on systems that don't have
 * catopen/catgets (like, say, SunOS 4.1.3).
 */

typedef void *nl_catd;
#define catopen(name, oflags) (NULL)
#define catgets(catd, set_num, msg_num, s) (s)