
static char rcsid[] ="@(#)$Id: lock.c,v 5.6 1993/01/19 03:55:39 syd Exp $";

/*******************************************************************************
 *  The Elm Mail System  -  $Revision: 5.6 $   $State: Exp $
 *
 * 			Copyright (c) 1988-1992 USENET Community Trust
 * 			Copyright (c) 1986,1987 Dave Taylor
 *******************************************************************************
 * Bug reports, patches, comments, suggestions should be sent to:
 *
 *	Philip Brown    filter@bolthole.com
 *
 *******************************************************************************
 * 2000/01/14 Philip Brown
 * cleaned up NEEDS_LOCK_FD define, and did some paranoia lock releasing
 * 2000/01/12 Philip Brown
 *  patch sent in by Jim Brown, for maildir error
 * 2000/01/08 Philip Brown
 * fixed bug in return check for errno, in Grab_the_file()
 * 1999/11/23 Philip Brown
 * converted nasty hardcodes, to use user_mailbox[]
 *
 * $Log: lock.c,v $
 * Revision 5.6  1993/01/19  03:55:39  syd
 * exitprog.c makes a reference to a null character pointer, savecopy.c
 * tries to reference an uninitialized variable, and the previous patch to
 * src/lock.c to get rid of an uninitialized variable compiler message
 * needed to be put in filter/lock.c as well.
 * From: wdh@grouper.mkt.csd.harris.com (W. David Higgins)
 *
 * Revision 5.5  1992/12/07  03:49:49  syd
 * use BSD or not apollo on file.h include as its not valid
 * for Apollos under sys5.3 compile type
 * From: gordonb@mcil.comm.mot.com (Gordon Berkley)
 *
 * Revision 5.4  1992/10/17  22:35:33  syd
 * Change lock file name to add user name on filter locking of mail spool
 * From: Peter Brouwer <pb@idca.tds.philips.nl>
 *
 * Revision 5.3  1992/10/12  00:25:52  syd
 * Lock error codes (fcntl vs flock) were backwards
 * From: howardl@wb3ffv.ampr.org (Howard Leadmon - WB3FFV)
 *
 * Revision 5.2  1992/10/11  00:52:11  syd
 * Switch to wrapper for flock and fcntl locking.
 * Change order to fcntl first, other order blocked.
 * From: Jukka Ukkonen <ukkonen@csc.fi>
 *
 * Revision 5.1  1992/10/03  22:18:09  syd
 * Initial checkin as of 2.4 Release at PL0
 *
 *
 ******************************************************************************/


/** The lock() and unlock() routines herein duplicate exactly the
    equivalent routines in the Elm Mail System, and should also be
    compatible with sendmail, rmail, etc etc.

  
**/

#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include "defs.h"
#include "filter.h"
#include "s_filter.h"
#include "utils.h"
#include "lock.h"
#include "audit.h"

static  int  we_locked_it;
static char *lockfile=(char*)0;

extern char *mk_lockname();

#ifdef	USE_FLOCK_LOCKING
#define	NEEDS_LOCK_FD
#include <sys/types.h>
#  if (defined(BSD) || !defined(apollo))
#    include <sys/file.h>
#  endif
#endif

#ifdef USE_FCNTL_LOCKING
#define	NEEDS_LOCK_FD
static struct flock lock_info;
#endif

#ifdef NEEDS_LOCK_FD
static	int flock_fd = -1;
#endif

#ifdef	USE_DOTLOCK_LOCKING			/*  USE_DOTLOCK_LOCKING	*/
static	char dotlock_name[SLEN];
#endif

#define	FLOCKING_OK	0
#define	FLOCKING_RETRY	1
#define	FLOCKING_FAIL	-1

extern  int  errno;

/* This is the subroutine that does the actual OS-level locking.
 * Called by flock_file ()
 */
int
Grab_the_file(file_fd)
int file_fd;
{
    int	retcode	= FLOCKING_OK;

    errno = 0;

#ifdef   USE_FCNTL_LOCKING /* similar to lockf(), kinda */
    lock_info.l_type = F_WRLCK;
    lock_info.l_whence = 0;
    lock_info.l_start = 0;
    lock_info.l_len = 0;

    if (fcntl(file_fd, F_SETLK, &lock_info)) {
	return ((errno == EACCES) || (errno == EAGAIN))
		? FLOCKING_RETRY
		: FLOCKING_FAIL ;
    }
#endif

#ifdef	USE_FLOCK_LOCKING
    if (flock (file_fd, LOCK_NB | LOCK_EX)) {

	retcode = ((errno == EWOULDBLOCK) || (errno == EAGAIN))
		   ? FLOCKING_RETRY
		   : FLOCKING_FAIL ;

#ifdef USE_FCNTL_LOCKING
	lock_info.l_type = F_UNLCK;
	lock_info.l_whence = 0;
	lock_info.l_start = 0;
	lock_info.l_len = 0;

	/*
	 *  Just unlock it because we did not succeed with the
	 *  flock()-style locking. Never mind the return value.
	 *  It was our own lock anyway if we ever got this far.
	 */
	fcntl (file_fd, F_SETLK, &lock_info);
#endif
	return retcode;
    }
#endif

    return retcode;
}

int
Release_the_file(file_fd)
int file_fd;
{
int	fcntlret = 0,
	flockret = 0,
	fcntlerr = 0;
#ifdef	USE_FLOCK_LOCKING
	int flockerr = 0;

	errno = 0;
	flockret = flock (file_fd, LOCK_UN);
	flockerr = errno;
#endif

#ifdef	USE_FCNTL_LOCKING
	lock_info.l_type = F_UNLCK;
	lock_info.l_whence = 0;
	lock_info.l_start = 0;
	lock_info.l_len = 0;

	errno = 0;
	fcntlret = fcntl (file_fd, F_SETLK, &lock_info);
	fcntlerr = errno;
#endif

	if (fcntlret) {
		errno = fcntlerr;
		return fcntlret;
	}
	else if (flockret) {
		errno = flockret;
		return flockret;
	}
	return 0;
}


#ifdef	USE_DOTLOCK_LOCKING			/* { USE_DOTLOCK_LOCKING	*/
/* check for if lockfile exists. If it does, then check if
 * process is still there. If not, REMOVE lockfile!
 * Return 0 if things okay to go ahead, or 1 otherwise.
 */
int clearoldlock(char *fname)
{
#ifdef PIDCHECK
	int lockfd;
	char pid_buffer[SHORT];
	int lockpid;
#endif

	if(access(fname, F_OK) != 0)
		return 0; /* no lockfile! */

#ifdef PIDCHECK
	/** first, try to read the lock file, and if possible, check the pid.
	    If we can validate that the pid is no longer active, then remove
	    the lock file.
	**/
	if((lockfd=open(lockfile,O_RDONLY)) == -1)
		return 1; /* cant read lockfile?!! */

	if (read(lockfd, pid_buffer, SHORT) > 0) {
		lockpid = atoi(pid_buffer);
		if (lockpid) {
			if (kill(lockpid, 0)) {
				close(lockfd);
				if (unlink(lockfile) == 0)
					return(0);
			} 
		}
	}
#endif /* PIDCHECK */

	return 1;
}
#endif /* USE_DOTLOCK_LOCKING */

/* Badly named. This encapsulates FLOCK AND LOCKF
 * Disclaimer: This was moved out from the existing lock() stuff, so
 *   it's still rather icky. It sets/uses the STATIC fd, 'flock_fd' -- phil
 *
 * NEVER TRY TO HAVE MORE THAN ONE FILE LOCKED AT A TIME, using
 * the routines in here, since they use a single static fd to do it.
 * You need to unlock whatever you have locked, before locking something
 * else.
 *
 * Called by both lock(), and lockfolder()
 *
 * Return 0 on successful lock, 1 otherwise
 */
#ifdef	NEEDS_LOCK_FD
int flock_file(fname)
char *fname;
{
	int attempts, ret=0;

	/* Using the static flock_fd is kinda icky. Hopefully,
	 * we'll only ever have one file open at a time anyways, so
	 * it should always work out. But JUST IN CASE...
	 */
	if(flock_fd != -1){
		/* This should auto-unlock. I cant be bothered to ifdef,
		 * this whole clause is a hack anyway
		 */
		close(flock_fd);
	}

	flock_fd = open(fname, O_RDWR | O_CREAT, 0600);

	if ( flock_fd >= 0 ) {
	    for (attempts = 0; attempts < 10; attempts++) {

		ret = Grab_the_file (flock_fd);
	    
		if ((ret == FLOCKING_FAIL) || (ret == FLOCKING_OK)) {
		    break;
		}
		sleep((unsigned)3);
	    }

	    if ( ret == FLOCKING_OK ) {
		we_locked_it++;
		ret = 0;
	    }
	    else {
		we_locked_it = 0;
		(void)close(flock_fd);
		flock_fd=-1;
		ret = 1;
	    }
	}

	return ret;
}
#endif 			/* NEEDS_LOCK_FD	*/

/* uses static-global flock_fd, via flock_file */
/* See flock_file(), about never holding more than one file locked */
/* return 1 on successfully lock, 0 otherwise */
int
lockfolder(foldername)
char *foldername; 
{
	char *lockfilename;
#ifdef	USE_DOTLOCK_LOCKING			/* { USE_DOTLOCK_LOCKING	*/
	int len=strlen(foldername);
	lockfilename=(char*)safemalloc(len+6);
	sprintf(lockfilename,"%s.lock", foldername);
	if(clearoldlock(lockfilename) != 0)
	{
		return 0;
	}

#endif /* USE_DOTLOCK_LOCKING */
#ifdef	NEEDS_LOCK_FD			/* { NEEDS_LOCK_FD	*/
	if(flock_file(foldername) != 0){
#ifdef	USE_DOTLOCK_LOCKING		/* { USE_DOTLOCK_LOCKING	*/
		(void)unlink(lockfilename);
		free(lockfilename);
#endif					/* } USE_DOTLOCK_LOCKING	*/
		return 0;
	}
	
#endif					 /* { NEEDS_LOCK_FD	*/
	if(lockfilename!=NULL)
		free(lockfilename);

	return 1; /* Locked file successfully */
}

/* uses static-global flock_fd, via flock_file */
/* This needs to be similar to unlock () */
void unlockfolder(foldername)
char *foldername;
{
	char *lockfilename;
#ifdef	USE_DOTLOCK_LOCKING			/* { USE_DOTLOCK_LOCKING*/
	int len=strlen(foldername);
	lockfilename=(char*)safemalloc(len+6);
	sprintf(lockfilename,"%s.lock", foldername);
	unlink(lockfilename);
	free(lockfilename);
#endif /* USE_DOTLOCK_LOCKING */
#ifdef	NEEDS_LOCK_FD
	Release_the_file (flock_fd);
	(void)close(flock_fd);
	flock_fd = -1;
#endif
}

/** This routine will return 1 if we could lock the (PRIMARY!) mailfile,
    zero otherwise.
**/
int
lock()
{
	int attempts = 0, lockfd;
	int ret=1; /* default to "good", set bad on fail */
	char maildir[SLEN];
	char *slash=rindex(user_mailbox,'/');
	if(slash==NULL){
		strcpy(maildir, mailhome);
	}  else {
		stringcopy(maildir, user_mailbox, slash - user_mailbox +2);
	}

#ifdef	USE_DOTLOCK_LOCKING			/* { USE_DOTLOCK_LOCKING	*/
	stringcopy(dotlock_name,maildir, SLEN);
	strncat(dotlock_name, username, SLEN);
	lockfile = mk_lockname(dotlock_name);

	if(clearoldlock(lockfile) != 0){
		if(outfptr != NULL){
			fprintf(outfptr,
				catgets(elm_msg_cat,FilterSet,
				 FilterCantOpenTempFile2,
				"filter (%s): clearoldlock failed for %s!!\n"),
				date_n_user(),lockfile);
		}
		return 0;
	}

	while((lockfd = open(lockfile, O_WRONLY | O_CREAT | O_EXCL, 0444)) <0 
	       && attempts++ < 10) {
	  sleep(3);	/* wait three seconds each pass, okay?? */
	}

	if (lockfd >= 0) {
	  we_locked_it++;
	  close(lockfd);			/* no need to keep it open! */
	} else {
	  if(outfptr != NULL)
		fprintf(outfptr,
			catgets(elm_msg_cat,FilterSet,
			    FilterCantOpenTempFile2,
			    "filter (%s): open lock file failed for %s!!\n"),
			date_n_user(), lockfile);
		
	  ret = 0;
	}
	  
#endif					/* } USE_DOTLOCK_LOCKING	*/

#ifdef	NEEDS_LOCK_FD			/* { NEEDS_LOCK_FD	*/
	if(flock_file(user_mailbox) != 0) {
		fprintf(outfptr,
			catgets(elm_msg_cat,FilterSet,
			    FilterCantOpenTempFile2,
			    "filter (%s): flock failed for %s, errno %d!!\n"),
			date_n_user(), user_mailbox, errno);
		
		ret = 0;
#ifdef	USE_DOTLOCK_LOCKING		/* { USE_DOTLOCK_LOCKING	*/
	    (void)unlink(lockfile);
#endif					/* } USE_DOTLOCK_LOCKING	*/
	}
	
#endif					 /* { NEEDS_LOCK_FD	*/
	return(ret);
}

void unlock()
{
	/** this routine will remove the lock file, but only if we were
	    the people that locked it in the first place... **/

#ifdef	USE_DOTLOCK_LOCKING
	if (we_locked_it && lockfile[0]) {
	    unlink(lockfile);	/* blamo! */
	    lockfile[0] = 0;
	}
#endif
#ifdef	NEEDS_LOCK_FD
	if (we_locked_it && flock_fd >= 0) {
	    /*
	     *	Give it at least a decent try. OK?
	     */
	    (void) Release_the_file (flock_fd);
	    /*
	     *	And then blast the locks anyway...
	     */
	    (void) close (flock_fd);
	    flock_fd = -1;
	}
#endif
	we_locked_it = 0;
}
