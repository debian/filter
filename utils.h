
#ifdef ANSI_C

extern int contains(char *str, char *pat);
extern int expand_filename(char *filename);
extern char *itoa(int i, int two_digit);
extern void leave(char *reason);
extern void log_msg(int what);
extern void lowercase(char *string);
extern FILE* make_tempfile();
extern void printlist(FILE *fptr, LIST *listing);
extern void remove_tempfile();
extern char *safemalloc(int bsize);
extern void stringcopy(char *dest, char *src, int len);
#endif
