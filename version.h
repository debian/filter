/* Now that "filter" is separate from the elm distribution, we need
 * a versionstring for it.
 * May as well start with "v2.0"
 */

static char *versionstring="filter 2.6.3";

/* This string exists for 'catopen()', the LOCALE utility, and needs to be 
   plainer than versionstring
 */
static char *versioncat="filter2.0";
